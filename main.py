import sqlite3
import openpyxl
import pandas as pd
import re


# df1 = pd.read_excel('Referentiel_npec.xlsx', sheet_name="Onglet 2 - global", skiprows = 3)
df2 = pd.read_excel('Referentiel_npec.xlsx', sheet_name="Onglet 3 - référentiel NPEC", skiprows= 3)
df3 = pd.read_excel('Referentiel_npec.xlsx', sheet_name="Onglet 4 - CPNE-IDCC", skiprows = 2)
# df1.columns = df1.columns.str.strip()
df2.columns = df2.columns.str.strip()
df3.columns = df3.columns.str.strip()
# print(df1.columns)
#df1.columns = ["Code RNCP, Nom_certif, Certificateur, Diplome"]
#print(df2.columns)
#print(df3.columns)
df2 = df2.rename(columns={"Certificateur* \n(*liste non exhaustive - premier dans la fiche RNCP)": 'Certificateur'})
df2.columns = ['Code RNCP', 'Libelle', 'Certificateur',
       'Diplome', 'Code CPNE', 'CPNE', 'NPEC final', 'Statut',
       "Date"]
df2['Date'] = df2['Date'].astype(str)
#print(df2.columns)

df2bis = df2[['Code CPNE', 'CPNE']]

df2bis.loc[:,'Code CPNE'] = df2bis['Code CPNE'].astype(int)
# data_CPNE= df2bis.values.tolist()
# print(df2bis['Code CPNE'].value_counts())
# print(df2bis.info())
df2ter = df2[['Code RNCP', 'Libelle', 'Certificateur', 'Diplome']]

df2ter.loc[:,'Code RNCP'] = df2ter['Code RNCP'].astype(str)
df2ter.loc[:,'Libelle'] = df2ter['Libelle'].astype(str)
df2ter.loc[:,'Certificateur'] = df2ter['Certificateur'].astype(str)
df2ter.loc[:,'Diplome'] = df2ter['Diplome'].astype(str)


df2quatro = df2[['NPEC final', 'Statut', 'Date']]
df2quatro.loc[:,'NPEC final'] = df2quatro['NPEC final'].astype(float)
df2quatro.loc[:,'Statut'] = df2quatro['Statut'].astype(str)
df2quatro.loc[:,'Date'] = df2quatro['Date'].astype(str)

#print(df2quatro['NPEC final'].value_counts())
#print(df2quatro.info())
df3.loc[:,'IDCC'] = df3['IDCC'].astype(int)
# print(df3['IDCC'].value_counts())
# print(df3.info())



con = sqlite3.connect('rncp.db')

cur = con.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS CPNE(code_cpne Integer primary key, nom TEXT)")

cur.execute("""CREATE TABLE IF NOT EXISTS RNCP(
            id INTEGER PRIMARY KEY,
            code_rncp TEXT, 
            intitule_certification TEXT, 
            certification TEXT, 
            libelle_diplome TEXT
            )""")

cur.execute("CREATE TABLE IF NOT EXISTS RNCP_CPNE(code_rncp Text, code_cpne Integer, montant float, statut CHECK (statut IN ('A', 'CPNE', 'R')) NOT NULL DEFAULT 'A', date Text)")

cur.execute("CREATE TABLE IF NOT EXISTS IDCC(code_cpne Integer, idcc Integer)")


for index, row in df2bis.iterrows():
    cur.execute("INSERT OR IGNORE INTO CPNE (code_cpne, nom) VALUES(?, ?)", (row['Code CPNE'], row['CPNE']))
    code_cpne = cur.lastrowid 
    

for index, row in df2ter.iterrows():
    row = row.to_dict()
    print(row)
    cur.execute("""
                INSERT OR IGNORE INTO RNCP 
                (code_rncp, intitule_certification, certification, libelle_diplome)
                VALUES(?, ?, ?, ?)""", 
                (row['Code RNCP'], row['Libelle'], row['Certificateur'], row['Diplome']))
    code_rncp = cur.lastrowid

print(row['Code RNCP'])
print(row['Libelle'])
print(row['Certificateur'])
print(row['Diplome'])

for index, row in df2quatro.iterrows():
    cur.execute("INSERT INTO RNCP_CPNE (code_rncp, code_cpne, montant, statut, date) VALUES(?, ?, ?, ?, ?)", [code_rncp, code_cpne, row['NPEC final'], row['Statut'], row['Date']])
   

for index, row in df3.iterrows():
    cur.execute("INSERT INTO IDCC (code_cpne, idcc) VALUES(?, ?)", [code_cpne, row['IDCC']])
   



# new_con = sqlite3.connect("rncp.db")

# new_cur = new_con.cursor()

# for row in new_cur.execute("SELECT * FROM CPNE"):
#     print(row)

con.commit()
con.close()