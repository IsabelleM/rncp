from typing import Union
from fastapi import FastAPI
import sqlite3
from flask import Flask, jsonify
#from sqlite3 import execute_query, connect_sqlite

app = FastAPI()
con = sqlite3.connect('rncp.db', check_same_thread = False)

@app.get("/head/")
def print_head():
    return ['Code RNCP', 'Libelle', 'Certificateur',
       'Diplome', 'Code CPNE', 'CPNE', 'NPEC final', 'Statut',
       "Date"]

@app.get("/code_rncp/{intitule_certification}")
def recup_rncp(intitule_certification:str):
    cursor = con.cursor()
    query = ("""SELECT intitule_certification FROM RNCP WHERE intitule_certification = ?""")
    cursor.execute( query, ('intitule_certification',))
    result = cursor.fetchall()
    return result


# @app.get("/datas/{search}")
# def search_datas(search: str, page: int, page_size: int):
#     search = '%' + search + '%'
#     result = []
#     offset = (page - 1) * page_size

#     query = (""" SELECT code_rncp, intitule_certification, certification, libelle_diplome, code_cpne, nom, montant, statut, date, idcc
#             FROM RNCP
#             JOIN RNCP_CPNE ON RNCP.code_rncp = RNCP_CPNE.code_rncp
#             JOIN CPNE ON RNCP_CPNE.code_cpne = CPNE.code_cpne
#             JOIN IDCC ON CPNE.code_cpne = IDCC.code_cpne
#             WHERE code_rncp LIKE ?
#             OR intitule_certification LIKE ?
#             OR certification LIKE ?
#             OR libelle_diplome LIKE ?
#             OR code_cpne LIKE ?
#             OR nom LIKE ?
#             OR montant LIKE ?
#             OR statut LIKE ?
#             OR date LIKE ?
#             OR idcc LIKE ?
#         """)
#     params = (search, search, search, search, search, search, search, search, search, page_size, offset)
#     list_results = query(query, con, params, fetch=True)
#     for row in list_results:
#         temp = {"code_rncp": row[0], 
#                 "intitule_certification": row[1], 
#                 "certification": row[2],
#                 "libelle_diplome": row[3],
#                 "code_cpne": row[4],
#                 "nom": row[5],
#                 "montant": row[6],
#                 "statut": row[7],
#                 "date": row[8],
#                 "idcc":row[9]}
#         result.append(temp)
#     return result


#app = Flask(__name__)

@app.get('/api/all_data')
def get_all_data():
    
        # Se connecter à la base de données
        con = sqlite3.connect('rncp.db')
        cur = con.cursor()

        # Exécuter la requête pour récupérer toutes les données
        cur.execute("SELECT * FROM CPNE")
        cpne_data = cur.fetchall()

        cur.execute("SELECT * FROM RNCP")
        rncp_data = cur.fetchall()

        cur.execute("SELECT * FROM RNCP_CPNE")
        rncp_cpne_data = cur.fetchall()

        cur.execute("SELECT * FROM IDCC")
        idcc_data = cur.fetchall()

        # Fermer la connexion à la base de données
        con.close()

        # Créer un dictionnaire pour stocker toutes les données
        all_data = {
            'CPNE': cpne_data,
            'RNCP': rncp_data,
            'RNCP_CPNE': rncp_cpne_data,
            'IDCC': idcc_data
        }

        # Retourner les données au format JSON
        return jsonify(all_data)


if __name__ == '__main__':
    app.run(debug=True)